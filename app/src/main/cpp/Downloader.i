// run comand
// swig.exe -c++ -java -package com.example.downloader.downloader -outdir Downloader\app\src\main\java\com\example\downloader\downloader Downloader.i

%module(directors="1") Downloader_Wrapper
%feature("director") CListener;

%{
#include "message.h"	
#include "downloader.h"
#include "listener.h"
%}

%include "listener.h"
%include "std_string.i"
%include "downloader.h"
%include "message.h"