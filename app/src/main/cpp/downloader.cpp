#include "downloader.h"
#include "session.h"

CDownloader::CDownloader()
{
	// bind signal to slot
	download_complete.connect(boost::bind(&CDownloader::OnDownloadComplete, this, _1, _2));
}

CDownloader::~CDownloader()
{

}

CDownloader* CDownloader::Instance()
{
    static CDownloader slf;
    return &slf;
}

void CDownloader::OnDownloadComplete(string sUrl, string sFileName)
{
	// Send message to the listeners
	shared_ptr<CMessage> pMsg(new CMessage());
	
	lock_guard<mutex> lock(mSessionsLock);
	auto iter = std::find_if(m_Sessions.begin(), m_Sessions.end(),
	[=](CSession* s)
	{
		return (s->m_sUrl == sUrl && s->m_sFileName == sFileName);
	});

	if (iter != m_Sessions.end())
	{
		CSession* s = *iter;
		pMsg->m_bSuccessfull = s->m_bSuccessfull;
		m_Sessions.erase(iter);
		delete s;
	}

	pMsg->m_bComplete = true;
	pMsg->m_sUrl = sUrl;
	pMsg->m_sFileName = sFileName;
	Notify(pMsg);
}

void CDownloader::Download(string sUrl, string sFileName)
{
	thread([=]
	{
		CSession* pSession = new CSession(sUrl, sFileName);
		mSessionsLock.lock();
		this->m_Sessions.push_back(pSession);
		mSessionsLock.unlock();
		bool bRet = pSession->Download(m_sDownloadPath);		

		// emit the signal
		download_complete(pSession->m_sUrl, pSession->m_sFileName);
	}).detach();
}

void CDownloader::AddListener(CListener* pListener)
{
	mListenersLock.lock();
	if (std::find(m_Listeners.begin(), m_Listeners.end(), pListener) == m_Listeners.end())
	{
		m_Listeners.push_back(pListener);
	}
	mListenersLock.unlock();
}

void CDownloader::Notify(shared_ptr<CMessage> pMsg)
{
	mListenersLock.lock();
	for (auto listener : m_Listeners)
	{
		listener->Send(pMsg);
	}
	mListenersLock.unlock();
}