#ifndef DOWNLOADER_DOWNLOADER_H
#define DOWNLOADER_DOWNLOADER_H

#include <boost/signals2.hpp>
#include <string>
#include <vector>
#include <mutex>
#include "listener.h"
#include "message.h"
#include "session.h"

using namespace std;

class CDownloader
{
public:
    static CDownloader* Instance();
    void SetDownloadPath(string sPath) {m_sDownloadPath = sPath;}
    void Download(string sUrl, string sFileName);
	void AddListener(CListener* pListener);
	void Notify(shared_ptr<CMessage> pMsg);
	void OnDownloadComplete(string sUrl, string sFileName);
    ~CDownloader();

private:
	boost::signals2::signal<void(string, string)> download_complete;
	mutex mListenersLock;
	mutex mSessionsLock;
	vector<CListener*> m_Listeners;
	vector<CSession*> m_Sessions;
    string m_sDownloadPath;
    CDownloader();
};


#endif //DOWNLOADER_DOWNLOADER_H
