//
// Created by moskvich on 17.11.2017.
//

#include "listener.h"

void CListener::ProcessQueue()
{
	while (!m_bStop)
	{
		auto msg = mQueue.pop();
		OnMessage(*msg.get());
	}
}

CListener::~CListener()
{
	if (m_pProcessQueueThread)
	{
		m_pProcessQueueThread->join();
	}
}

void CListener::Send(shared_ptr<CMessage> pMsg)
{
	mQueue.push(pMsg);
}