//
// Created by moskvich on 17.11.2017.
//

#ifndef DOWNLOADER_LISTENER_H
#define DOWNLOADER_LISTENER_H
#include <functional>
#include <thread>
#include "safequeue.h"
#include "message.h"

using namespace std;

class CListener
{
public:
	virtual ~CListener();
	virtual void OnMessage(CMessage msg) {}
	void Send(shared_ptr<CMessage> pMsg);
	CListener()
	{
		m_bStop = false;
		m_pProcessQueueThread = new thread(&CListener::ProcessQueue, this);
	}

private:
	
	CSafequeue<shared_ptr<CMessage>> mQueue;
	thread* m_pProcessQueueThread;
	void ProcessQueue();
	bool m_bStop;
};


#endif //DOWNLOADER_LISTENER_H
