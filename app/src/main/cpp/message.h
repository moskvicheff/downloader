//
// Created by moskvich on 17.11.2017.
//

#ifndef DOWNLOADER_MESSAGE_H
#define DOWNLOADER_MESSAGE_H

#include <string>
using namespace std;

class CMessage
{
public:

	CMessage()
	{
		m_bComplete = false;
		m_bSuccessfull = false;
		m_nPerCent = 0;
	}
	~CMessage() {}

	string m_sUrl;
	string m_sFileName;
	bool m_bComplete;
	bool m_bSuccessfull;
	int m_nPerCent;
};


#endif //DOWNLOADER_MESSAGE_H
