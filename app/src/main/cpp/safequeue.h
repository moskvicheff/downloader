//
// Created by moskvich on 17.11.2017.
//

#ifndef DOWNLOADER_SAFEQUEUE_H
#define DOWNLOADER_SAFEQUEUE_H

#include <queue>
#include <mutex>
#include <condition_variable>

using namespace std;

template <class T>
class CSafequeue
{
public:
	CSafequeue(void)
		: mQueue()
		, mMutex()
		, mCondVariable()
	{}

	~CSafequeue(void)
	{}

	// Add an element to the queue.
	void push(T t)
	{
		lock_guard<std::mutex> lock(mMutex);
		mQueue.push(t);
		mCondVariable.notify_one();
	}

	// Get the "front"-element.
	// If the queue is empty, wait till a element is avaiable.
	T pop(void)
	{
		unique_lock<std::mutex> lock(mMutex);
		while (mQueue.empty())
		{
			// release lock as long as the wait and reaquire it afterwards.
			mCondVariable.wait(lock);
		}
		T val = mQueue.front();
		mQueue.pop();
		return val;
	}

private:
	std::queue<T> mQueue;
	mutable std::mutex mMutex;
	std::condition_variable mCondVariable;
};


#endif //DOWNLOADER_SAFEQUEUE_H
