#include "session.h"
#include <boost/asio/ssl/stream.hpp>
#include <boost/asio/connect.hpp>

#include <boost/beast/version.hpp>
#include <boost/beast/core/flat_buffer.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/read.hpp>
#include <fstream>
#include "url.hpp"
#include <iostream>
#include "downloader.h"

namespace ssl = boost::asio::ssl;
namespace http = boost::beast::http;

bool GetHost(const string& sUrl, string& sHost)
{
    sHost.clear();

    Url url(sUrl);
    sHost = url.host();

    return !sHost.empty();
}

bool GetTarget(const string& sUrl, string& sTarget)
{
    Url url(sUrl);
    sTarget = url.path();

    return !sTarget.empty();
}

bool IsSecure(const string& sUrl)
{
    Url url(sUrl);
    return boost::starts_with(url.scheme() + "://", "https://");
}

bool GetPort(const string& sUrl, string& sPort)
{
	Url url(sUrl);
	string port = url.port();
	if (IsSecure(sUrl))
	{
		sPort = port.empty() ? "443" : port;
	}
	else
	{
		sPort = port.empty() ? "80" : port;
	}

	return true;
}

CSession::CSession(string sUrl, string sFileName)
: m_sUrl(sUrl)
, m_sFileName(sFileName)
, m_bSuccessfull(false)
{

}

bool CSession::Download(string sPathToSave)
{
    if (IsSecure(m_sUrl))
    {
        return DownloadSecure(sPathToSave);
    }

    try
    {
        boost::asio::io_service ios;
        tcp::socket socket{ios};
        tcp::resolver resolver{ios};

        string sHost, sPort, sTarget;

        if (!(GetHost(m_sUrl, sHost) && GetPort(m_sUrl, sPort) && GetTarget(m_sUrl, sTarget)))
        {
            return false;
        }

        tcp::resolver::query query(sHost, sPort);
        auto const results = resolver.resolve(query);

        boost::system::error_code ec;

        boost::asio::connect(socket, results, ec);

        if (ec)
        {
			throw boost::system::system_error{ ec };
        }

        http::request<http::string_body> req{http::verb::get, sTarget, 11};
        req.set(http::field::host, sHost);
        req.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);

        http::write(socket, req);

		// Open file for download
		string sFilePath = sPathToSave;
		if (!boost::ends_with(sPathToSave, "/"))
		{
			sFilePath.append("/");
		}
		sFilePath.append(m_sFileName);
		std::ofstream ofs(sFilePath, std::ofstream::out | std::ofstream::binary);      


		boost::asio::streambuf response;
		boost::asio::read_until(socket, response, "\r\n");

		// Check that response is OK.
		std::istream response_stream(&response);
		std::string http_version;
		response_stream >> http_version;
		unsigned int status_code;
		response_stream >> status_code;

		if (status_code != 200)
		{
			return false;
		}

		std::string status_message;
		std::getline(response_stream, status_message);

		// Read http headers
		boost::asio::read_until(socket, response, "\r\n\r\n");

		// Look for content length header
		uint64_t nBytesReceived = 0;
		unsigned long nContentLength = 0;
		string header;
		while (std::getline(response_stream, header) && header != "\r")
		{
			if (boost::starts_with(header, "Content-Length:"))
			{
				stringstream ss(header.substr(16));
				ss >> nContentLength;
			}
		}

		if (nContentLength > 0)
		{
			nBytesReceived += response.size();
			// Write content bytes from response
			if (nBytesReceived > 0)
			{
				ofs << &response;
			}
			int nPercentCompleted = 0;
			int nPercentReported = 0;
			// Read until EOF
			while (nBytesReceived < nContentLength)
			{
				unsigned long nRead = boost::asio::read(socket, response, boost::asio::transfer_at_least(1), ec);
				if (!nRead)
				{
					throw boost::system::system_error{ ec };
				}
				nBytesReceived += nRead;
				ofs << &response;

				nPercentCompleted = nBytesReceived * 100 / nContentLength;

				if (nPercentCompleted > nPercentReported)
				{
					// Update percent for this download
					shared_ptr<CMessage> pMsg(new CMessage());
					pMsg->m_sUrl = m_sUrl;
					pMsg->m_sFileName = m_sFileName;
					pMsg->m_nPerCent = nPercentCompleted;
					CDownloader::Instance()->Notify(pMsg);
					nPercentReported = nPercentCompleted;
				}
			}
		}
    }
    catch(std::exception const& e)
    {
		auto what = e.what();
        return false;
    }

	m_bSuccessfull = true;
    return true;
}

bool CSession::DownloadSecure(string sPathToSave)
{
    try
    {
        ssl::context sslContext{ssl::context::sslv23_client};
        boost::asio::io_service ios;

        ssl::stream <tcp::socket> stream{ios, sslContext};

        if (!SSL_set_tlsext_host_name(stream.native_handle(), m_sUrl.c_str()))
        {
            boost::system::error_code ec{static_cast<int>(::ERR_get_error()), boost::asio::error::get_ssl_category()};
            throw boost::system::system_error{ec};
        }

        string sHost, sPort, sTarget;

        if (!(GetHost(m_sUrl, sHost) && GetPort(m_sUrl, sPort) && GetTarget(m_sUrl, sTarget)))
        {
			return false;
        }

        tcp::resolver resolver{ios};
        tcp::resolver::query query(sHost, sPort);
        auto const results = resolver.resolve(query);

        boost::system::error_code ec;

        boost::asio::connect(stream.next_layer(), results, ec);

        if (ec)
        {
			throw boost::system::system_error{ ec };
        }

        stream.handshake(ssl::stream_base::client);

        http::request<http::string_body> req{http::verb::get, sTarget, 11};
        req.set(http::field::host, sHost);
        req.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);

        http::write(stream, req);

        
		// Open file for download
		string sFilePath = sPathToSave;
		if (!boost::ends_with(sPathToSave, "/"))
		{
			sFilePath.append("/");
		}
		sFilePath.append(m_sFileName);
		std::ofstream ofs(sFilePath, std::ofstream::out | std::ofstream::binary);


		boost::asio::streambuf response;
		boost::asio::read_until(stream, response, "\r\n");

		// Check that response is OK.
		std::istream response_stream(&response);
		std::string http_version;
		response_stream >> http_version;
		unsigned int status_code;
		response_stream >> status_code;

		if (status_code != 200)
		{
			return false;
		}

		string status_message;
		getline(response_stream, status_message);

		// Read http headers
		boost::asio::read_until(stream, response, "\r\n\r\n");

		// Look for content length header
		unsigned long nContentLength = 0;
		uint64_t nBytesReceived = 0;
		string header;
		while (std::getline(response_stream, header) && header != "\r")
		{
			if (boost::starts_with(header, "Content-Length:"))
			{
				stringstream ss(header.substr(16));
				ss >> nContentLength;
			}
		}

		if (nContentLength > 0)
		{
			nBytesReceived += response.size();
			// Write content bytes from response
			if (nBytesReceived > 0)
			{
				ofs << &response;
			}
			int nPercentCompleted = 0;
			int nPercentReported = 0;
			// Read until EOF
			while (nBytesReceived < nContentLength)
			{
				size_t nRead = boost::asio::read(stream, response, boost::asio::transfer_at_least(1), ec);
				if (!nRead)
				{
					throw boost::system::system_error{ ec };
				}
				nBytesReceived += nRead;
				ofs << &response;

				nPercentCompleted = nBytesReceived * 100 / nContentLength;

				if (nPercentCompleted > nPercentReported)
				{
					// Update percent for this download
					shared_ptr<CMessage> pMsg(new CMessage());
					pMsg->m_sUrl = m_sUrl;
					pMsg->m_sFileName = m_sFileName;
					pMsg->m_nPerCent = nPercentCompleted;
					CDownloader::Instance()->Notify(pMsg);
					nPercentReported = nPercentCompleted;
				}
			}
		}
       

        stream.shutdown(ec);
    }
    catch(std::exception const& e)
    {
        return false;
    }
	m_bSuccessfull = true;
    return true;
}

CSession::~CSession()
{

}