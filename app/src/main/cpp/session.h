#ifndef DOWNLOADER_SESSION_H
#define DOWNLOADER_SESSION_H

#include <boost/beast/http.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <string>

using namespace std;
using tcp = boost::asio::ip::tcp;

class CSession
{
public:
    CSession(string sUrl, string sFileName);
    ~CSession();

	string m_sUrl;		// holds download URL
	string m_sFileName;	// filename for this download

    bool Download(string sPathToSave); // Starts download operation
	bool m_bSuccessfull;

private:
    bool DownloadSecure(string sPathToSave);
};


#endif //DOWNLOADER_SESSION_H
