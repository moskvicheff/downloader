#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"
#include "downloader.h"
#include "session.h"
TEST_CASE( "Downloader can be instantiated", "[CDownloader]" )
{
	CDownloader* pDownloader = CDownloader::Instance();
	REQUIRE(pDownloader != 0);
}
TEST_CASE ( "Session must fail on an invalid input data", "[CSession]")
{
	SECTION("Invalid download URI")
	{
		CSession session("invalid_uri", "");
		REQUIRE(session.Download("") == false);
	}
	SECTION ("Empty filename")
	{
		CSession session("http://google.com/index.html", "");
		REQUIRE(session.Download("") == false);
	}
	SECTION ("Empty both URI and filename")
	{
		CSession session("", "");
		REQUIRE(session.Download("") == false);
	}
}
