/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 3.0.12
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.example.downloader.downloader;

public class CListener {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected CListener(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(CListener obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        Downloader_WrapperJNI.delete_CListener(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  protected void swigDirectorDisconnect() {
    swigCMemOwn = false;
    delete();
  }

  public void swigReleaseOwnership() {
    swigCMemOwn = false;
    Downloader_WrapperJNI.CListener_change_ownership(this, swigCPtr, false);
  }

  public void swigTakeOwnership() {
    swigCMemOwn = true;
    Downloader_WrapperJNI.CListener_change_ownership(this, swigCPtr, true);
  }

  public void OnMessage(CMessage msg) {
    if (getClass() == CListener.class) Downloader_WrapperJNI.CListener_OnMessage(swigCPtr, this, CMessage.getCPtr(msg), msg); else Downloader_WrapperJNI.CListener_OnMessageSwigExplicitCListener(swigCPtr, this, CMessage.getCPtr(msg), msg);
  }

  public void Send(SWIGTYPE_p_shared_ptrT_CMessage_t pMsg) {
    Downloader_WrapperJNI.CListener_Send(swigCPtr, this, SWIGTYPE_p_shared_ptrT_CMessage_t.getCPtr(pMsg));
  }

  public CListener() {
    this(Downloader_WrapperJNI.new_CListener(), true);
    Downloader_WrapperJNI.CListener_director_connect(this, swigCPtr, swigCMemOwn, true);
  }

}
