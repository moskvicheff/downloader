package com.example.downloader.downloader;

/**
 * Created by moskvich on 21.11.2017.
 */

public class DownloadListItem
{
    public int mPercent = 0;
    public String mFileName = "";
    boolean mIsComplete = false;
    boolean mIsSuccessfull = false;
    public DownloadListItem(int nPercent, String sFileName, boolean bIsComplete, boolean bIsSuccessfull)
    {
        mPercent = nPercent;
        mFileName = sFileName;
        mIsComplete = bIsComplete;
        mIsSuccessfull = bIsSuccessfull;
    }
}
