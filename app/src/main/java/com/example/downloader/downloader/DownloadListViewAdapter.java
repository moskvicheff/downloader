package com.example.downloader.downloader;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by moskvich on 21.11.2017.
 */

public class DownloadListViewAdapter extends BaseAdapter
{
    Context context;
    List<DownloadListItem> data = new ArrayList<>();
    private static LayoutInflater inflater = null;

    public void AddOrUpdate(DownloadListItem i)
    {
        for (DownloadListItem item : data)
        {
            if (item.mFileName.equals(i.mFileName))
            {
                data.set(data.indexOf(item), i);
                return;
            }
        }
        data.add(i);
    }
    public DownloadListViewAdapter(Context c)
    {
        this.context = c;
        inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View vi = convertView;
        if (vi == null)
        {
            vi = inflater.inflate(R.layout.list_view, null);
        }
        DownloadListItem item = data.get(position);
        TextView text = (TextView) vi.findViewById(R.id.text);
        ProgressBar pb = (ProgressBar) vi.findViewById(R.id.progressBar);
        text.setText(item.mFileName);

        if (item.mIsComplete)
        {
            pb.setProgress(100);
            vi.setBackgroundColor(item.mIsSuccessfull? Color.GREEN : Color.RED);
        }
        else
        {
            pb.setProgress(item.mPercent);
        }
        return vi;
    }
}
