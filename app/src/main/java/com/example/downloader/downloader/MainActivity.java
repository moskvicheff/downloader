package com.example.downloader.downloader;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.support.constraint.solver.widgets.ConstraintHorizontalLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public class MyListener extends CListener
    {
        @Override
        public void OnMessage(final CMessage msg)
        {
            runOnUiThread(new Runnable() {
                @Override
                public void run()
                {
                    mActivity.OnDownloadMessage(msg);
                }
            });
        }
    }

    private MyListener mListener = new MyListener();
    private ArrayList<LinearLayout> mListItems = new ArrayList<LinearLayout>();
    private DownloadListViewAdapter mListViewAdapter = null;
    public MainActivity mActivity = null;
    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    public void OnDownloadMessage(CMessage msg)
    {
        int nPercent = msg.getM_nPerCent();
        boolean bComplete = msg.getM_bComplete();
        boolean bSuccessfull = msg.getM_bSuccessfull();
        String sFileName = msg.getM_sFileName();
        String sUrl = msg.getM_sUrl();

        if (!sFileName.isEmpty())
        {
            mListViewAdapter.AddOrUpdate(new DownloadListItem(nPercent, sFileName, bComplete, bSuccessfull));
            mListViewAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        mActivity = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ListView lw = (ListView) mActivity.findViewById(R.id.ListView);

        mListViewAdapter = new DownloadListViewAdapter(this);
        lw.setAdapter(mListViewAdapter);
        Button b = (Button) findViewById(R.id.button);

        b.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Context context = mActivity;
                final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Add download");

                final LinearLayout ll = new LinearLayout(context);
                ll.setOrientation(LinearLayout.VERTICAL);
                final EditText editUrl = new EditText(context);
                editUrl.setHint("URL");
                final EditText fileName = new EditText(context);
                fileName.setHint("file name");
                ll.addView(editUrl);
                ll.addView(fileName);
                builder.setView(ll);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        String url = editUrl.getText().toString();
                        String filename = fileName.getText().toString();
                        CDownloader.Instance().Download(url, filename);
                        dialogInterface.dismiss();
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        dialogInterface.cancel();
                    }
                });
                builder.show();
            }
        });


        CDownloader.Instance().AddListener(mListener);
        //downloader.Download("https://www.iso.org/files/live/sites/isoorg/files/archive/pdf/en/annual_report_2009.pdf", "annual_report_2009.pdf");
    }
}
